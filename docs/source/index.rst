Welcome to SkillCorner's Python SDK Documentation
=================================================

Useful Links
------------

* **Code**: `skillcorner GitLab <https://gitlab.com/public-corner/skillcorner/>`_
* **Pypi**: `skillcorner PyPI <https://pypi.org/project/skillcorner/>`_
* **SwaggerUI API Documentation**: `REST API Docs <https://www.skillcorner.com/api/docs>`_

Overview
--------

The ``SkillcornerClient`` is a Python class designed to interact with the Skillcorner API. It provides methods to retrieve and save various data related to competitions, seasons, matches, teams, and players.

Contents
--------

.. toctree::
   :maxdepth: 2

   getting_started
   skillcorner_client
